import React, { Component } from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Route, Link, Switch } from 'react-router-dom';
import Home from './Home/Home';
import Modal from './Modal/Modal';
import Modal2 from './Modal2/Modal2';
import Modal3 from './Modal3/Modal3';

import Diario from './Diario/Diario';


class App extends Component {
  constructor(props){
    super(props);
  }
 
  componentDidMount(){
  }
  render() {
    return (
      <BrowserRouter>

        <div className="App">
      
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/diario/acordar/2/3" exact component={Modal3}/>
          <Route path="/diario/acordar/2" exact component={Modal2}/>
          <Route path="/diario/acordar" exact component={Modal}/>
          <Route path="/diario/treino/2" exact component={Modal2}/>
          <Route path="/diario/treino" exact component={Modal}/>
          <Route path="/diario/almocoNaTiaCecilia" exact component={Modal}/>
          <Route path="/diario/almocoNaTiaCecilia/2" exact component={Modal2}/>
          <Route path="/diario/final" exact component={Modal}/>
          <Route path="/diario/final/2" exact component={Modal2}/>



          <Route path="/diario" exact component={Diario}/>

        </Switch>
       

        </div>

      </BrowserRouter>

    );
  }
}

export default App;


