import React, {Component} from 'react';
import './Diario.css';
import {Link} from 'react-router-dom';
import Modal from '../Modal/Modal'

class Diario extends Component{
    constructor(props){
        super(props);
        this.onClickHandler = this.onClickHandler.bind(this);
        this.state = {
            teste: 1,
        }
    }
    onClickHandler(){
        this.setState((state) =>{
            return ({teste: 2})
        });
        console.log('fasdf');
    }
    render(){
        return(
            <div>

                <div className="container">
                    <h2 className="py-4" style={{fontSize:'40px', fontWeight:'700'}}>DOMINGO</h2>
                    <div className="row row-do-evento align-items-center">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">06:00</h6>
                        <div className="col-10 row-da-hora evento"></div>
                    </div>

                    <div className="row row-do-evento align-items-center row-com-evento color-2 text-white">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">08:00</h6>
                        <Link className="col-10 row-da-hora evento text-white" to={{pathname:'/diario/acordar', state:{deOndeVeio:'acordar'}}} >Acordar</Link>
                    </div>

                    <div className="row row-do-evento align-items-center color-1 text-white">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">10:00</h6>
                        <Link className="col-10 row-da-hora evento text-white" to={{pathname:'/diario/treino', state:{deOndeVeio:'treino'}} }>Treino</Link>
                    </div>
                    <div className="row row-do-evento align-items-center color-3">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">12:00</h6>
                        <Link className="col-10 row-da-hora evento" to={{pathname:'/diario/almocoNaTiaCecilia', state:{deOndeVeio:'almocoNaTiaCecilia'}}}>Almoço na Tia Cecilia</Link>
                    </div>
                    <div className="row row-do-evento align-items-center">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">14:00</h6>
                        <div className="col-10 row-da-hora evento"></div>
                    </div>
                    <div className="row row-do-evento align-items-center color-1 text-white">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">16:00</h6>
                        <div className="col-10 row-da-hora evento">Palestra UCL::dev.hire("Facebook")</div>
                    </div>
                    <div className="row  row-do-evento align-items-center">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">18:00</h6>
                        <div className="col-10 row-da-hora evento"></div>
                    </div>
                    <div className="row row-do-evento align-items-center">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">20:00</h6>
                        <div className="col-10 row-da-hora evento"></div>
                    </div>
                    <div className="row row-do-evento align-items-center color-2">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">22:00</h6>
                        <Link className="col-10 row-da-hora evento text-white" to={{pathname:'/diario/final', state:{deOndeVeio:'final'}} }>Game of Thrones / Festa</Link>
                    </div>
                    <div className="row row-do-evento align-items-center">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">23:00</h6>
                        <div className="col-10 row-da-hora evento"></div>
                    </div>
                    <div className="row row-do-evento align-items-center">
                        <h6 className="col-2 row-da-hora m-0 hora h-100">00:00</h6>
                        <div className="col-10 row-da-hora evento"></div>
                    </div>
                </div>
            </div>
        )
    }

}


export default Diario;