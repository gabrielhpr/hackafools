

import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Home extends Component{

    constructor(props){
        super(props);
        this.state ={
            tipo: '11'
        }
        console.log(props);
    }
   
    render(){
        return(
            <div className="container-fluid">
       <h2 className="py-4" style={{fontSize:'40px', fontWeight:'700'}}>ABRIL</h2>
       <ul className="row justify-content-center">
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Dom</li>
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Seg</li>
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Ter</li>
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Qua</li>
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Qui</li>
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Sext</li>
           <li className="d-inline-block calendar-days col-1 color-2 text-white">Sab</li>
         </ul>

        <ul className="row justify-content-center mb-0">
           <li className="d-inline-block calendar-days col-1"></li>
           <li className="d-inline-block calendar-days col-1 dias-passados">1</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">2</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">3</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">4</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">5</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">6</li>
         </ul>

         <ul className="row justify-content-center mb-0">
           <li className="d-inline-block calendar-days col-1 dias-passados">7</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">8</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">9</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">10</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">11</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">12</li>
           <li className="d-inline-block calendar-days col-1 dias-passados">13</li>
         </ul>

         <ul className="row justify-content-center mb-0">
           <li className="d-inline-block calendar-days col-1 color-1 text-white"><Link className="btn text-white p-0"  to="/diario" style={{fontSize:'30px'}}>14</Link></li>
           <li className="d-inline-block calendar-days col-1">15</li>
           <li className="d-inline-block calendar-days col-1">16</li>
           <li className="d-inline-block calendar-days col-1">17</li>
           <li className="d-inline-block calendar-days col-1">18</li>
           <li className="d-inline-block calendar-days col-1">19</li>
           <li className="d-inline-block calendar-days col-1">20</li>
         </ul>

         <ul className="row justify-content-center mb-0">
           <li className="d-inline-block calendar-days col-1">21</li>
           <li className="d-inline-block calendar-days col-1">22</li>
           <li className="d-inline-block calendar-days col-1">23</li>
           <li className="d-inline-block calendar-days col-1">24</li>
           <li className="d-inline-block calendar-days col-1">25</li>
           <li className="d-inline-block calendar-days col-1">26</li>
           <li className="d-inline-block calendar-days col-1">27</li>
         </ul>

         <ul className="row justify-content-center mb-0">
           <li className="d-inline-block calendar-days col-1">28</li>
           <li className="d-inline-block calendar-days col-1">29</li>
           <li className="d-inline-block calendar-days col-1">30</li>
           <li className="d-inline-block calendar-days col-1"></li>
           <li className="d-inline-block calendar-days col-1"></li>
           <li className="d-inline-block calendar-days col-1"></li>
           <li className="d-inline-block calendar-days col-1"></li>
         </ul>

         

       </div>
        )
    }
}

export default Home;