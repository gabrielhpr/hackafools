import React,{Component} from 'react';
import './Modal.css';
import ModalTipo from '../UI/UI';


class Modal extends Component{
    constructor(props){
        super(props);
        console.log(props);
        const deOndeVeio = this.props.location.state.deOndeVeio;
        this.deOndeVeio = deOndeVeio;

        
        this.state ={
            cardGen:{
                eventonome:'',
                eventotexto:'',
                eventopergunta:'Voce vai acordar ??????'

            },
            acordar:{
                eventonome:'Hora de acordar',
                eventotexto:'A CASA ESTÁ PEGANDO FOGO LEVANTA !',
                eventopergunta:'Voce vai acordar ??????',
            },
            treino:{
                eventonome:'TREINO',
                eventotexto:'TÁ ARREPENDIDO DE NÃO TER IDO TREINAR ?',
                eventopergunta:'',

            },
            almocoNaTiaCecilia:{
                eventonome:'ALMOÇO NA TIA CECILIA',
                eventotexto:'Você vai almoçar com a tia Cecilia?',
                eventopergunta:'',
            },
            final:{
                eventonome:'Game of Thrones ou Festa',
                eventotexto:'WARNING!!! Você tem dois eventos na mesma hora',
                eventopergunta:'Ir a festa ????',
            }

        }
    }
    

    onClickHandle1(nomeDeOndeVeio){
        if(nomeDeOndeVeio == 'acordar'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'acordar/2', value:true} });
        }
        else if(nomeDeOndeVeio == 'treino'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'treino/2', value:true} });
        }
        else if(nomeDeOndeVeio == 'almocoNaTiaCecilia'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'almocoNaTiaCecilia/2', value:true} });
        }
        else if(nomeDeOndeVeio == 'final'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'final/2', value:true} });
        }
        
    }
    onClickHandle2(nomeDeOndeVeio){
        if(nomeDeOndeVeio == 'acordar'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'acordar/2', value:false} });
        }  
        else if(nomeDeOndeVeio == 'treino'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'treino/2', value:false} });
        } 
        else if(nomeDeOndeVeio == 'final'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'final/2', value:false} });
        }
        else if(nomeDeOndeVeio == 'almocoNaTiaCecilia'){
            this.props.history.push({pathname: this.props.location.pathname + '/2', state:{deOndeVeio:'almocoNaTiaCecilia/2', value:false} });
        }
    }  


 render(){
     return(
         <div style={{width:'100%'}}>
            {(this.deOndeVeio == 'acordar') ? <ModalTipo eventonome={this.state.acordar.eventonome} eventotexto={this.state.acordar.eventotexto} eventopergunta={this.state.acordar.eventopergunta} click1={() => this.onClickHandle1('acordar')} click2={() => this.onClickHandle2('acordar')}> <img src={require('../assets/images/ACORDAR.gif')} /></ModalTipo> : console.log('s') } 
            {(this.deOndeVeio == 'treino') ? <ModalTipo eventonome={this.state.treino.eventonome} eventotexto={this.state.treino.eventotexto} eventopergunta={this.state.treino.eventopergunta} click1={() => this.onClickHandle1('treino')} click2={()=> this.onClickHandle1('treino')}></ModalTipo> : console.log('s') } 
            {(this.deOndeVeio == 'almocoNaTiaCecilia') ? <ModalTipo eventonome={this.state.almocoNaTiaCecilia.eventonome} eventotexto={this.state.almocoNaTiaCecilia.eventotexto} eventopergunta={this.state.almocoNaTiaCecilia.eventopergunta} click1={() => this.onClickHandle1('almocoNaTiaCecilia')} click2={()=> this.onClickHandle2('almocoNaTiaCecilia')}><img src={require('../assets/images/tia.gif')} /></ModalTipo> : console.log('s') } 
            {(this.deOndeVeio == 'final') ? <ModalTipo eventonome={this.state.final.eventonome} eventotexto={this.state.final.eventotexto} eventopergunta={this.state.final.eventopergunta} click1={() => this.onClickHandle1('final')} click2={()=> this.onClickHandle2('final')}><img src={require('../assets/images/festa.gif')} /></ModalTipo> : console.log('s') } 

         </div>
     )
 }
}

export default Modal;