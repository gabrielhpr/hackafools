import React,{Component} from 'react';
import './Modal2.css';
import ModalTipo from '../UI/UI';


class Modal2 extends Component{
    constructor(props){
        super(props);
        console.log(props);
        const deOndeVeio = this.props.location.state.deOndeVeio;
        this.deOndeVeio = deOndeVeio;
        const value = this.props.location.state.value;
        this.value = value;

        
        this.state ={
            cardGen:{
                eventonome:'',
                eventotexto:'',
                eventopergunta:'Você vai acordar ??????'

            },
            acordar2true:{
                eventonome:'Hora de acordar',
                eventotexto:'Você tem festa mais tarde',
                eventopergunta:'Vai pro treino mesmo ????',
            },
            acordar2false:{
                eventonome:'Hora de acordar',
                eventotexto:'Mas e o seu treino ????',
                eventopergunta:'Vai procrastinar mesmo ??????',
            },
            treino2:{
                eventonome:'TREINO',
                eventotexto:'Agora o treinador está cagando para você, seu bunda mole',
                eventopergunta:'',

            },
            almocoNaTiaCecilia2:{
                eventonome:'ALMOCO NA TIA CECILIA',
                eventotexto:'A gente sabia que você não ia mesmo, fica com o seu miojão',
                eventopergunta:'',

            },
            final2True:{
                eventonome:'',
                eventotexto:'',
                eventopergunta:'',
            },
            final2False:{
                eventonome:'',
                eventotexto:'',
                eventopergunta:'',
            },

        }
    }
    

    onClickHandle1(nomeDeOndeVeio){
        if(nomeDeOndeVeio == 'acordar/2'){
            this.props.history.push({pathname: this.props.location.pathname + '/3', state:{deOndeVeio:'acordar/2/3', value:true} });
        }
        else if(nomeDeOndeVeio == 'treino/2'){
            this.props.history.push('/diario');

        }
        else if(nomeDeOndeVeio == 'almocoNaTiaCecilia/2'){
            this.props.history.push('/diario');

        }
        else if(nomeDeOndeVeio == 'final/2'){
            this.props.history.push('/diario');

        }
    }
    onClickHandle2(){
    }

 render(){
     return(
         <div style={{width:'100%'}}>
            {( (this.deOndeVeio == 'acordar/2') && this.value == true)  ? <ModalTipo eventonome={this.state.acordar2true.eventonome} eventotexto={this.state.acordar2true.eventotexto} eventopergunta={this.state.acordar2true.eventopergunta} click1={() => this.onClickHandle1('acordar/2')} click2={this.onClickHandle2}></ModalTipo> : console.log('s') } 
            {( (this.deOndeVeio == 'acordar/2') && this.value == false)  ? <ModalTipo eventonome={this.state.acordar2false.eventonome} eventotexto={this.state.acordar2false.eventotexto} eventopergunta={this.state.acordar2false.eventopergunta} click1={() => this.onClickHandle1('acordar/2')} click2={this.onClickHandle2}></ModalTipo> : console.log('s') } 

            {(this.deOndeVeio == 'treino/2') ? <ModalTipo t2="/OK"  show2="d-none" eventonome={this.state.treino2.eventonome} eventotexto={this.state.treino2.eventotexto} eventopergunta={this.state.treino2.eventopergunta} click1={() => this.onClickHandle1('treino/2')} click2={this.onClickHandle2}><img src={require('../assets/images/fogo.gif')} /> </ModalTipo> : console.log('s') } 


            {(this.deOndeVeio == 'almocoNaTiaCecilia/2') ? <ModalTipo t2="/OK"  show2="d-none" eventonome={this.state.almocoNaTiaCecilia2.eventonome} eventotexto={this.state.almocoNaTiaCecilia2.eventotexto} eventopergunta={this.state.almocoNaTiaCecilia2.eventopergunta} click1={() => this.onClickHandle1('almocoNaTiaCecilia/2')} click2={()=> this.onClickHandle2('almocoNaTiaCecilia/2')}></ModalTipo> : console.log('s') } 
            {( (this.deOndeVeio == 'final/2') && this.value == true ) ? <ModalTipo t2="/OK"  show="d-none" show2="d-none" eventonome={this.state.final2True.eventonome} eventotexto={this.state.final2True.eventotexto} eventopergunta={this.state.final2True.eventopergunta} click1={() => this.onClickHandle1('final/2')} click2={()=> this.onClickHandle2('final/2')}><img src={require('../assets/images/festeiro.png')} /></ModalTipo> : console.log('s') } 
            {( (this.deOndeVeio == 'final/2') && this.value == false ) ? <ModalTipo t2="/OK"  show="d-none" show2="d-none" eventonome={this.state.final2False.eventonome} eventotexto={this.state.final2False.eventotexto} eventopergunta={this.state.final2False.eventopergunta} click1={() => this.onClickHandle1('final/2')} click2={()=> this.onClickHandle2('final/2')}><img src={require('../assets/images/nerd.png')} /></ModalTipo> : console.log('s') } 

            <button>fechar</button>
         </div>
     )
 }
}

export default Modal2;