
import React from 'react';
import {Link} from 'react-router-dom';

const ModalTipo = (props) =>{
    return(
        <div className="container-fluid principal">
        
        <div className="row card-evento bg-white mx-auto " style={{height: (props.children) ? '100vh': ''}} >
        

            <h2 className="col-12 p-3 text-center" style={{fontSize:'35px'}}>{props.eventonome}</h2>
            <div className="mx-auto">
                    {(props.children) ? props.children : console.log('fs')}

                </div>
            <p className="col-12" style={{fontSize:'30px', fontWeight:'500'}}>{props.eventotexto}</p>
            <p className="col-12" style={{fontSize:'30px', fontWeight:'500'}}>{props.eventopergunta}</p>


            <div className="col-12 justify-content-center">
                <button className={ "btn btn-dark col-3 p-3 botao-estilo "+ props.show } onClick={props.click1}>SIM {props.t2}</button>
                <button className={ "btn btn-danger col-3 p-3 botao-estilo "+ props.show2} onClick={props.click2}>NOP</button>

            </div>
        </div>
        
     </div>
    )
}
export default ModalTipo;